"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const moleculer_1 = require("moleculer");
const winston = tslib_1.__importStar(require("winston"));
// Made possible by esModuleInterop
const moment_timezone_1 = tslib_1.__importDefault(require("moment-timezone"));
const { extend } = moleculer_1.Logger;
const accounts_1 = tslib_1.__importDefault(require("@pcmrbotjs/accounts"));
const account_manager_1 = tslib_1.__importDefault(require("@pcmrbotjs/account-manager"));
const proxy_1 = tslib_1.__importDefault(require("@pcmrbotjs/proxy"));
const reddit_1 = tslib_1.__importDefault(require("@pcmrbotjs/reddit"));
const reddit_rt_1 = tslib_1.__importDefault(require("@pcmrbotjs/reddit-rt"));
const slack_1 = tslib_1.__importDefault(require("@pcmrbotjs/slack"));
const bot_automod_1 = tslib_1.__importDefault(require("@pcmrbotjs/bot-automod"));
const bot_modmail_alert_1 = tslib_1.__importDefault(require("@pcmrbotjs/bot-modmail-alert"));
const bot_modqueue_alert_1 = tslib_1.__importDefault(require("@pcmrbotjs/bot-modqueue-alert"));
const bot_removal_alert_1 = tslib_1.__importDefault(require("@pcmrbotjs/bot-removal-alert"));
const bot_popular_post_alert_1 = tslib_1.__importDefault(require("@pcmrbotjs/bot-popular-post-alert"));
const bot_spoiler_review_1 = tslib_1.__importDefault(require("@pcmrbotjs/bot-spoiler-review"));
const bot_sticky_scheduler_1 = tslib_1.__importDefault(require("@pcmrbotjs/bot-sticky-scheduler"));
const bridge_unflaired_1 = tslib_1.__importDefault(require("@pcmrbotjs/bridge-unflaired"));
const usernote_pruner_1 = tslib_1.__importDefault(require("@pcmrbotjs/usernote-pruner"));
const broker = new moleculer_1.ServiceBroker({
    transporter: "TCP",
    namespace: "pcmrbot.js",
    logger: bindings => extend(winston.createLogger({
        format: winston.format.combine(winston.format.label({ label: bindings.mod }), winston.format.timestamp(), winston.format.colorize(), winston.format.printf(({ level, message, label, timestamp }) => {
            return `[${moment_timezone_1.default(timestamp).format("HH:mm:ss.SSS")}] ${level} ${label.toUpperCase()}: ${message}`;
        })),
        transports: [
            new winston.transports.Console()
        ]
    })),
    cacher: "Memory"
});
accounts_1.default(broker);
account_manager_1.default(broker);
proxy_1.default(broker);
reddit_1.default(broker);
reddit_rt_1.default(broker);
slack_1.default(broker);
bot_automod_1.default(broker);
bot_modmail_alert_1.default(broker);
bot_modqueue_alert_1.default(broker);
bot_popular_post_alert_1.default(broker);
bot_removal_alert_1.default(broker);
bot_spoiler_review_1.default(broker);
bot_sticky_scheduler_1.default(broker);
bridge_unflaired_1.default(broker);
usernote_pruner_1.default(broker);
broker.start();
