const { ServiceBroker } = require("moleculer");

const broker = new ServiceBroker({
	logFormatter: "simple",
	transporter: "TCP",
	namespace: "pcmrbot.js"
});

broker.createService({
	name: "test",
	events: {
		"**"(payload, sender, event) {
			console.log(payload, `Sender - ${sender}, Event - ${event}`);
		}
	},
	actions: {
		test: {
			async handler(ctx) {
				return await this.broker.call("v3.reddit.user.getMe", {}, {
					meta: {userId: "U1J0VSGAV",
					serviceType: "slack"}
				})
			}
		}
	}
})

broker.repl();
broker.start();
