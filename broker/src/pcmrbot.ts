import { ServiceBroker, Logger } from "moleculer";
import * as winston from "winston";
// Made possible by esModuleInterop
import moment from "moment-timezone";
const { extend } = Logger;

import registerAllAccountsServices from "@pcmrbotjs/accounts";
import registerAllAccountManagerServices from "@pcmrbotjs/account-manager";
import registerAllProxyServices from "@pcmrbotjs/proxy";
import registerAllRedditServices from "@pcmrbotjs/reddit";
import registerAllRedditRtServices from "@pcmrbotjs/reddit-rt";
import registerAllSlackServices from "@pcmrbotjs/slack";
import registerAllAutoModServices from "@pcmrbotjs/bot-automod";
import registerAllModmailAlertServices from "@pcmrbotjs/bot-modmail-alert";
import registerAllModqueueAlertServices from "@pcmrbotjs/bot-modqueue-alert";
import registerAllRemovalAlertServices from "@pcmrbotjs/bot-removal-alert";
import registerAllPopularPostAlertServices from "@pcmrbotjs/bot-popular-post-alert";
import registerAllSpoilerWatchServices from "@pcmrbotjs/bot-spoiler-review";
import registerAllStickySchedulerServices from "@pcmrbotjs/bot-sticky-scheduler";
import registerAllUnflairedBridgeServices from "@pcmrbotjs/bridge-unflaired";
import registerAllUsernotePrunerServices from "@pcmrbotjs/usernote-pruner";

const broker = new ServiceBroker({
    transporter: "TCP",
    namespace: "pcmrbot.js",
    logger: bindings => extend(winston.createLogger({
        format: winston.format.combine(
            winston.format.label({label: bindings.mod}),
            winston.format.timestamp(),
            winston.format.colorize(),
            winston.format.printf(({level, message, label, timestamp}) => {
                return `[${moment(timestamp).format("HH:mm:ss.SSS")}] ${level} ${label.toUpperCase()}: ${message}`
            })
        ),
        transports: [
            new winston.transports.Console()
        ]
    })),
    cacher: "Memory"
});

registerAllAccountsServices(broker);
registerAllAccountManagerServices(broker);
registerAllProxyServices(broker);
registerAllRedditServices(broker);
registerAllRedditRtServices(broker);
registerAllSlackServices(broker);
registerAllAutoModServices(broker);
registerAllModmailAlertServices(broker);
registerAllModqueueAlertServices(broker);
registerAllPopularPostAlertServices(broker);
registerAllRemovalAlertServices(broker);
registerAllSpoilerWatchServices(broker);
registerAllStickySchedulerServices(broker);
registerAllUnflairedBridgeServices(broker);
registerAllUsernotePrunerServices(broker);

broker.start();